#!/bin/bash
#
# Frederico Sales 
# <frederico.sales@engenharia.ufjf.br>
# 2018
#
clear;
docker volume create mosquitto_data;
docker run -itd \
	--name mosquitto \
	--restart on-failure \
	-p 1883:1883 \
	-p 9001:9001 \
	-v ${PWD}/mosquitto.conf:/mosquitto/config/mosquitto.conf \
	-v mosquitto_data:/mosquitto/data \
	-v mosquitto_data:/mosquitto/log \
	eclipse-mosquitto
